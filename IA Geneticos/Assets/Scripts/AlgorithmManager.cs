﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct StatsCarData
{
    public int generations;
    public string[] genomas;
    public int[] scores;
}

public class AlgorithmManager : MonoBehaviour
{
    public static AlgorithmManager instance;

    public int maxGenerationsWithoutChange = 20;
    public int currentGeneration = 0;
    public float waitTime = 0.5f;
    [Range(0, 0.4f)]
    public float percentageForTop5Offspring;
    int currentGensNoChange;
    float currentTime;
    bool endedTimer = true;
    bool showScores = true;

    float minFriction = 1.0f, maxFriction = 5.0f;

    public PhysicsMaterial2D terrainMaterial;

    GameObject[] orderedCars;
    CarData[] orderedData;
    CarData bestOfAllTimes;
    StatsCarData orderedStats;
    List<string> genomas;
    List<int> scores;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        genomas = new List<string>();
        scores = new List<int>();
        bestOfAllTimes = new CarData();
    }

    // Update is called once per frame
    void Update()
    {
        if (!endedTimer)
        {
            currentTime += Time.deltaTime;

            if (currentTime > waitTime)
            {
                endedTimer = true;
                if (currentGensNoChange < maxGenerationsWithoutChange)
                {
                    CarGenerator.GenerateDerivedGeneration(orderedData);
                }
                else
                {
                    CarGenerator.GenerateRandomGeneration();
                }
            }
        }
    }

    public void SetRandomFriction()
    {
        terrainMaterial.friction = Random.Range(minFriction,maxFriction);
    }

    public void SaveAndShowStats()
    {
        if(CarGenerator.carsLeft > 0)
        {
            return;
        }
        orderedCars = CarGenerator.GetOrderedCars();
        orderedData = new CarData[orderedCars.Length];
        orderedStats.generations = currentGeneration;
       
        for (int i = 0; i < orderedCars.Length; i++)
        {
            orderedData[i] = orderedCars[i].GetComponent<Car>().data;
        }

        CarData[] keyCars = new CarData[5];
        keyCars[0] = orderedData[0];
        keyCars[1] = orderedData[49];
        keyCars[2] = orderedData[99];
        keyCars[3] = orderedData[149];
        keyCars[4] = orderedData[199];

        for (int i = 0; i < orderedData.Length; i++)
        {
            genomas.Add(orderedData[i].genomaID);
            scores.Add(orderedData[i].score);
        }

        orderedStats.genomas = genomas.ToArray();
        orderedStats.scores = scores.ToArray();

        SaveManager.instance.SaveGeneration(orderedData, currentGeneration);
        SaveManager.instance.SaveKeyData(keyCars);
        SaveManager.instance.SaveStats(orderedStats);

        if (bestOfAllTimes.score < orderedData[0].score)
        {
            bestOfAllTimes = orderedData[0];
            currentGensNoChange = 0;
        }
        else
        {
            currentGensNoChange++;
        }

        SaveManager.instance.SaveBestOfAllTimes(bestOfAllTimes);
        
        if (showScores)
        {
            UIManager.instance.bestPanel.UpdateTexts(bestOfAllTimes);
            UIManager.instance.lastBestPanel.UpdateTexts(orderedData[0]);
        }

        currentGeneration++;
        StartNewGeneration();
    }

    public void StartNewGeneration()
    {
        if (currentGensNoChange < maxGenerationsWithoutChange)
        {
            CarData[] newGenData = new CarData[CarGenerator.objectsGenerated];
            int survivors = CarGenerator.objectsGenerated / 2;
            for (int i = 0; i < CarGenerator.objectsGenerated; i++)
            {
                int randomIndex1 = 0;
                if (Random.value < percentageForTop5Offspring)
                    randomIndex1 = Random.Range(0, 5);
                else
                    randomIndex1 = Random.Range(0, survivors);
                
                int randomIndex2;
                do
                {
                    randomIndex2 = Random.Range(0, survivors);
                } while (randomIndex2 == randomIndex1);
                CarData aux = GenesManager.instantce.CrossOver(orderedData[randomIndex1], orderedData[randomIndex2]);
                GenesManager.instantce.Mutate(ref aux);
                newGenData[i] = aux;
            }
            for (int i = 0; i < CarGenerator.objectsGenerated; i++)
            {
                orderedData[i] = new CarData();
                orderedData[i] = newGenData[i];
            }
        }
        ResetTimer();
    }
    void ResetTimer()
    {
        currentTime = 0;
        endedTimer = false;
    }
}
