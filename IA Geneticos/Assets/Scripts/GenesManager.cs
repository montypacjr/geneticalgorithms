﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenesManager : MonoBehaviour
{
    public static GenesManager instantce;

    [Tooltip("Start value of percentaje for posible mutability")]
    [Range(0.0f,1.0f)]
    public float MUTABILITY;
    

    void Awake()
    {
        instantce = this;
    }

    void Start()
    {
    }

    public CarData CrossOver(CarData parent1, CarData parent2)
    {
        CarData child = new CarData();
        child.ID = CarGenerator.GetID();
        int rng = Random.Range(0, 1);
        if (rng == 0)
        {
            child.vertexNumber = parent1.vertexNumber;
            child.colliderVertices = new Vector2[child.vertexNumber];
            child.carDensity = parent1.carDensity;
            for (int i = 0; i < child.vertexNumber; i++)
            {
                child.colliderVertices[i] = new Vector2(parent1.colliderVertices[i].x, parent1.colliderVertices[i].y);
            }
        }
        else
        {
            child.vertexNumber = parent2.vertexNumber;
            child.colliderVertices = new Vector2[child.vertexNumber];
            child.carDensity = parent2.carDensity;
            for (int i = 0; i < child.vertexNumber; i++)
            {
                child.colliderVertices[i] = new Vector2(parent2.colliderVertices[i].x, parent2.colliderVertices[i].y);
            }
        }
        rng = Random.Range(0, 1);
        if (rng == 0)
        {
            child.frontRadius = parent1.frontRadius;
            child.frontDensity = parent1.frontDensity;
        }
        else
        {
            child.frontRadius = parent2.frontRadius;
            child.frontDensity = parent2.frontDensity;
        }
        rng = Random.Range(0, 1);
        if (rng == 0)
        {
            child.frontOffset = parent1.frontOffset;
        }
        else
        {
            child.frontOffset = parent2.frontOffset;
        }
        rng = Random.Range(0, 1);
        if (rng == 0)
        {
            child.suspension = parent1.suspension;
        }
        else
        {
            child.suspension = parent2.suspension;
        }
        rng = Random.Range(0, 1);
        if (rng == 0)
        {
            child.motorSpeed = parent1.motorSpeed;
        }
        else
        {
            child.motorSpeed = parent2.motorSpeed;
        }
        rng = Random.Range(0, 1);
        if (rng == 0)
        {
            child.backRadius = parent1.backRadius;
            child.backDensity = parent1.backDensity;
        }
        else
        {
            child.backRadius = parent2.backRadius;
            child.backDensity = parent2.backDensity;
        }
        rng = Random.Range(0, 1);
        if (rng == 0)
        {
            child.backOffset = parent1.backOffset;
        }
        else
        {
            child.backOffset = parent2.backOffset;
        }
            
        child.Triangulate();

        return child;
    }

    public void Mutate(ref CarData childToMutate)
    {
        if (Random.value < MUTABILITY)//mutate vertex-dens
        {
            childToMutate.vertexNumber = Random.Range(CarGenerator.cdt.minVertexNumber, CarGenerator.cdt.maxVertexNumber);
            childToMutate.carDensity = childToMutate.vertexNumber;
            childToMutate.colliderVertices = CarGenerator.GenerateVertices(childToMutate.vertexNumber);
            childToMutate.Triangulate();
        }
        if (Random.value < MUTABILITY)//mutate suspension
        {
            childToMutate.suspension = Random.Range(CarGenerator.cdt.minSuspension, CarGenerator.cdt.maxSuspension);
        }
        if (Random.value < MUTABILITY)//mutate motor speed
        {
            childToMutate.motorSpeed = Random.Range(CarGenerator.cdt.minMotorSpeed, CarGenerator.cdt.maxMotorSpeed);
        }
        if (Random.value < MUTABILITY)//mutate front rad-dens
        {
            childToMutate.frontRadius = Random.Range(CarGenerator.cdt.minWheelRadius, CarGenerator.cdt.maxWheelRadius);
            float density = ((childToMutate.frontRadius - 0.2f) * 10);
            childToMutate.frontDensity = (float)System.Math.Round(density, 1);
        }
        if (Random.value < MUTABILITY)//mutate back rad-dens
        {
            childToMutate.backRadius = Random.Range(CarGenerator.cdt.minWheelRadius, CarGenerator.cdt.maxWheelRadius);
            float density = ((childToMutate.backRadius - 0.2f) * 10);
            childToMutate.backDensity = (float)System.Math.Round(density, 1);
        }
        if (Random.value < MUTABILITY)//mutate front offset
        {
            childToMutate.frontOffset = Random.Range(CarGenerator.cdt.minWheelOffset, CarGenerator.cdt.maxWheelOffset);
        }
        if (Random.value < MUTABILITY)//mutate back offset
        {
            childToMutate.backOffset = Random.Range(-CarGenerator.cdt.minWheelOffset, -CarGenerator.cdt.maxWheelOffset);
        }

        childToMutate.GenerateGenoma();
    }

}
