﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPool : MonoBehaviour {

    static List<GameObject> pooledObjects;

    public GameObject pooledObject;
    public int pooledAmount;
    static int initialPooledAmount;

    void Awake () {
        initialPooledAmount = pooledAmount;
	    InitializePool ();
    }

    public GameObject GetPooledObject(){
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }
	    return null;
    }
   
    public void ResetPool(){
	    for (int i = 0; i < pooledObjects.Count; i++) {
		    if(pooledObjects[i].activeInHierarchy){
			    pooledObjects [i].SetActive (false);
		    }
	    }
    }

    public static GameObject GetRandomObject()
    {
        int randomIndex = Random.Range(0,pooledObjects.Count);
        return pooledObjects[0];
    }

    public static GameObject GetObjectByScore()
    {
        GameObject car = GetRandomObject();
        int score = -10000;
        int currentScore;
        for (int i = 0; i < initialPooledAmount; i++)
        {
            Car c = pooledObjects[i].GetComponent<Car>();
            currentScore = c.GetScore();
            if (pooledObjects[i].activeInHierarchy && currentScore > score)
            {
                score = currentScore;
                car = pooledObjects[i];
            }
        }
        return car;
    }

    void InitializePool()
    {
        pooledObjects = new List<GameObject>();
        for (int i = 0; i < initialPooledAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(pooledObject, this.transform);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
    }
}

