﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuxiliarStrings
{
    public string id = "ID: ";
    public string genoma = "GENOMA: ";
    public string vertex = "Vertex: ";
    public string density = "Total Density: ";
    public string suspension = "Suspension: ";
    public string motorSpeed = "Motor Speed: ";
    public string maxDistance = "Max Distance: ";
    public string time = "Time spent: ";
    public string speed = "Speed: ", speedMagnitude = " m/s";
    public string score = "Score: ";
}

public class PanelManager : MonoBehaviour
{
    Text ID;
    Text genoma;
    Text vertex;
    Text density;
    Text suspension;
    Text motorSpeed;
    Text maxDistance;
    Text time;
    Text speed;
    Text score;

    AuxiliarStrings strings;

    // Start is called before the first frame update
    void Start()
    {
        strings = new AuxiliarStrings();
        Text[] textList = GetComponentsInChildren<Text>();

        ID = textList[1];
        genoma = textList[2];
        vertex = textList[3];
        density = textList[4];
        suspension = textList[5];
        motorSpeed = textList[6];
        maxDistance = textList[7];
        time = textList[8];
        speed = textList[9];
        score = textList[10];

    }

    public void UpdateTexts(CarData data)
    {
        ID.text = strings.id + data.ID.ToString();
        genoma.text = strings.genoma + data.genomaID;
        vertex.text = strings.vertex + data.vertexNumber.ToString();
        density.text = strings.density + (Mathf.RoundToInt(data.carDensity + data.frontDensity + data.backDensity)).ToString();
        suspension.text = strings.suspension + data.suspension.ToString();
        motorSpeed.text = strings.motorSpeed + data.motorSpeed.ToString();
        maxDistance.text = strings.maxDistance + data.maxDistance.ToString();
        time.text = strings.time + data.maxTime.ToString();
        speed.text = strings.speed + data.speed.ToString() + strings.speedMagnitude;
        score.text = strings.score + data.score.ToString();
    }
}
