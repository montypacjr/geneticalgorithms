﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    int currentPlay = 0;
    int currentGeneration;
    int usingPlayIndex = 0;
    bool created = false;
    public bool dontSave;

    public static SaveManager instance;

    void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!dontSave)
        {
            if (!Directory.Exists(Application.dataPath + "Saves"))
            {
                Directory.CreateDirectory(Application.dataPath + "Saves");
            }
            do
            {
                if (!Directory.Exists(Application.dataPath + "/Saves/Play" + currentPlay))
                {
                    Directory.CreateDirectory(Application.dataPath + "/Saves/Play" + currentPlay);
                    Directory.CreateDirectory(Application.dataPath + "/Saves/Play" + currentPlay + "/Generations");
                    Directory.CreateDirectory(Application.dataPath + "/Saves/Play" + currentPlay + "/KeyDatas");
                    Directory.CreateDirectory(Application.dataPath + "/Saves/Play" + currentPlay + "/StatsData");
                    usingPlayIndex = currentPlay;
                    created = true;
                }
                currentPlay++;
            } while (!created);
        }
    }

    public void SaveGeneration(CarData[] saveData, int currentGen)
    {
        if (!dontSave)
        {
            currentGeneration = currentGen;
            string dataAsJson = "";

            for (int i = 0; i < saveData.Length; i++)
            {
                dataAsJson += JsonUtility.ToJson(saveData[i], true);
            }

            string filePath = Application.dataPath + "/Saves/Play" + (currentPlay - 1) + "/Generations/Generation" + usingPlayIndex + "_" + currentGen + ".json";
            File.WriteAllText(filePath, dataAsJson);
        }
    }

    public void SaveKeyData(CarData[] keyData)
    {
        if (!dontSave)
        {
            string dataAsJson = "";

            for (int i = 0; i < 5; i++)
            {
                dataAsJson += JsonUtility.ToJson(keyData[i], true);
            }

            string filePath = Application.dataPath + "/Saves/Play" + (currentPlay - 1) + "/KeyDatas/KeyData" + usingPlayIndex + "_" + currentGeneration + ".json";
            File.WriteAllText(filePath, dataAsJson);
        }
    }

    public void SaveBestOfAllTimes(CarData best)
    {
        if (!dontSave)
        {
            string dataAsJson = "";
            
            dataAsJson += JsonUtility.ToJson(best, true);
            int generationToSave = currentGeneration + 1;
            dataAsJson += "{ \"Generation\": " + generationToSave.ToString() + " }" ;

            string filePath = Application.dataPath + "/Saves/Play" + (currentPlay - 1) + "/StatsData/BestOfAllTimes" + usingPlayIndex + "_" + currentGeneration + ".json";
            File.WriteAllText(filePath, dataAsJson);
        }
    }

    public void SaveStats(StatsCarData stats)
    {
        if (!dontSave)
        {
            string dataAsJson = JsonUtility.ToJson(stats, true);
            string filePath = Application.dataPath + "/Saves/Play" + (currentPlay - 1) + "/StatsData/OverallStats.json";
            File.WriteAllText(filePath, dataAsJson);
        }
    }
}
