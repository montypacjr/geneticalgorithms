﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public Text generation;
    public PanelManager bestPanel;
    public PanelManager lastBestPanel;

    void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        generation.text = "G: " + (AlgorithmManager.instance.currentGeneration + 1).ToString();
    }

    public void ResetTimeScale()
    {
        Time.timeScale = 1;
    }

    public void SetTimeScale2()
    {
        Time.timeScale = 2;
    }
    public void SetTimeScale5()
    {
        Time.timeScale = 5;
    }
    public void SetTimeScale10()
    {
        Time.timeScale = 10;
    }
}
