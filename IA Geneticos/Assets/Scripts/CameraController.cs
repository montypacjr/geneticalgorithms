﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public static CameraController instance;

    public GameObject objectToFollow;

    public float changeTime;
    float currentTime;

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        //objectToFollow = CarGenerator.GetRandomObject();
    }

    void LateUpdate()
    {
        if (CarGenerator.carsLeft > 0)
        {
            currentTime += Time.deltaTime;
            if (currentTime >= changeTime)
            {
                ChangeTarget();
                currentTime = 0;
            }
            if(objectToFollow != null)
                transform.position = new Vector3(Mathf.Lerp(transform.position.x, objectToFollow.transform.position.x, 0.1f), Mathf.Lerp(transform.position.y, objectToFollow.transform.position.y, 0.08f), transform.position.z);
        }
        else if (CarGenerator.carsLeft == 0)
        {
            transform.position = new Vector3(0,0, transform.position.z);
        }
        //Debug.Log(CarGenerator.carsLeft);
    }

    public void ChangeTarget()
    {
        objectToFollow = CarGenerator.GetObjectByDistance();
    }

}
