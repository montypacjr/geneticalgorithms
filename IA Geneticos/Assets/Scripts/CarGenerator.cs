﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDataThresholds
{
    //car data
    public int minVertexNumber = 3, maxVertexNumber = 7;
    public float minCarMass = 1, maxCarMass = 6;
    public float minPosX = -2, maxPosX = 2, minPosY = 0, maxPosY = 1;
    public float  maxPosXFirst = -1, minPosXSecond = 1, minPosYStart = -0.5f, maxPosYStart = 0;

    //suspension equal for front and back
    public float minSuspension = 0.4f, maxSuspension = 1;
    public int minMotorSpeed = 500, maxMotorSpeed = 1500;

    //front wheel data
    public float minWheelMass = 0.5f, maxWheelMass = 1;
    public float minWheelRadius = 0.4f, maxWheelRadius = 0.7f;
    public float minWheelOffset = 0.5f, maxWheelOffset = 1.5f;
    public int backWheelOffset = -1;
}

public class CarGenerator : MonoBehaviour
{
    public GameObject initialPrefab;
    public int maxObjectsPerGeneration;
    public static int objectsGenerated;
    static GameObject prefab;
    static Color[] colors;
    static GameObject[] cars;
    static int startY = 5;
    public static CarDataThresholds cdt = new CarDataThresholds();
    static int lastID = 0;
    public static int carsLeft;

    // Start is called before the first frame update
    void Start()
    {
        carsLeft = objectsGenerated = maxObjectsPerGeneration;
        colors = new Color[maxObjectsPerGeneration];
        cars = new GameObject[maxObjectsPerGeneration];
        prefab = initialPrefab;
        GenerateFirstGeneration();
    }

    public static GameObject GetRandomObject()
    {
        int randomIndex = Random.Range(0, cars.Length);
        return cars[randomIndex];
    }

    public static GameObject GetObjectByDistance()
    {
        GameObject car = GetRandomObject();
        int score = -10000;
        int currentScore;
        for (int i = 0; i < cars.Length; i++)
        {
            Car c = cars[i].GetComponent<Car>();
            currentScore = c.GetTotalDistance();
            if (cars[i].activeSelf && currentScore > score)
            {
                score = currentScore;
                car = cars[i];
            }
        }
        return car;
    }

    public static void ResetColors()
    {
        for (int i = 0; i < cars.Length; i++)
        {
            cars[i].GetComponent<Car>().SetColor(colors[i]);
        }
    }

    void GenerateFirstGeneration()
    {
        AlgorithmManager.instance.SetRandomFriction();
        for (int i = 0; i < maxObjectsPerGeneration; i++)
        {
            colors[i] = new Color(Random.value, Random.value, Random.value);

            CarData carData = GenerateRandomCar(colors[i]);
            cars[i] = (GameObject)Instantiate(initialPrefab, new Vector3(0, startY, 0), Quaternion.identity);
            cars[i].GetComponent<Car>().Initialize(carData);
            cars[i].SetActive(false);
        }
        ActivateCars();
    }
    public static void GenerateRandomGeneration()
    {
        DestroyAllCars();
        AlgorithmManager.instance.SetRandomFriction();
        for (int i = 0; i < objectsGenerated; i++)
        {
            colors[i] = new Color(Random.value, Random.value, Random.value);
            GameObject localPrefab = prefab;
            CarData carData = GenerateRandomCar(colors[i]);
            cars[i] = (GameObject)Instantiate(localPrefab, new Vector3(0, startY, 0), Quaternion.identity);
            cars[i].GetComponent<Car>().Initialize(carData);
            cars[i].SetActive(false);
        }
        ActivateCars();
    }

    public static void GenerateDerivedGeneration(CarData[] generation)
    {
        DestroyAllCars();
        for (int i = 0; i < objectsGenerated; i++)
        {
            GameObject localPrefab = prefab;
            cars[i] = (GameObject)Instantiate(localPrefab, new Vector3(0, startY, 0), Quaternion.identity);
            cars[i].GetComponent<Car>().Initialize(generation[i]);
            cars[i].SetActive(false);
        }
        ResetColors();
        ActivateCars();
    }

    static void DestroyAllCars()
    {
        for (int i = 0; i < objectsGenerated; i++)
        {
            Destroy(cars[i]);
        }
        carsLeft = objectsGenerated;
    }

    static void ActivateCars()
    {
        for (int i = 0; i < objectsGenerated; i++)
        {
            cars[i].SetActive(true);
        }
    }

    static CarData GenerateRandomCar(Color color)
    {
        CarData newCar = new CarData();

        newCar.ID = ++lastID;
        newCar.color = new Vector3(color.r, color.g, color.b);

        int vertexNumber = Random.Range(cdt.minVertexNumber, cdt.maxVertexNumber);
        newCar.vertexNumber = vertexNumber;
        newCar.colliderVertices = GenerateVertices(vertexNumber);
        newCar.Triangulate();
        newCar.carDensity = vertexNumber;

        newCar.suspension = Random.Range(cdt.minSuspension, cdt.maxSuspension);
        newCar.motorSpeed = Random.Range(cdt.minMotorSpeed, cdt.maxMotorSpeed);

        newCar.frontRadius = Random.Range(cdt.minWheelRadius, cdt.maxWheelRadius);
        newCar.frontOffset = Random.Range(cdt.minWheelOffset, cdt.maxWheelOffset);
        float density = ((newCar.frontRadius - 0.2f) * 10);
        //float density = newCar.frontRadius * 2;
        newCar.frontDensity = (float)System.Math.Round(density, 1);

        newCar.backRadius = Random.Range(cdt.minWheelRadius, cdt.maxWheelRadius);
        newCar.backOffset = Random.Range(-cdt.minWheelOffset, -cdt.maxWheelOffset);
        density = ((newCar.backRadius - 0.2f) * 10);
        //density = newCar.backRadius * 2;
        newCar.backDensity = (float)System.Math.Round(density, 1);

        newCar.GenerateGenoma();

        return newCar;
    }

    public static Vector2[] GenerateVertices(int vertexNumber)
    {
        Vector2[] positions = new Vector2[vertexNumber];
        for (int i = 0; i < vertexNumber; i++)
        {
            Vector2 vPosition;
            float x = 0;
            float y = 0;
            if (i == 0)
            {
                x = Random.Range(cdt.minPosX, cdt.maxPosXFirst);
                y = Random.Range(cdt.minPosYStart, cdt.maxPosYStart);
                vPosition = new Vector2(x, y);
                positions[i] = vPosition;
                continue;
            }
            else if (i == 1)
            {
                x = Random.Range(cdt.minPosXSecond, cdt.maxPosX);
                y = Random.Range(cdt.minPosYStart, cdt.maxPosYStart);
                vPosition = new Vector2(x, y);
                positions[i] = vPosition;
                continue;
            }
            x = Random.Range(cdt.minPosX, cdt.maxPosX);
            y = Random.Range(cdt.minPosY, cdt.maxPosY);
            vPosition = new Vector2(x, y);
            positions[i] = vPosition;
        }
        Vector2[] orderedPositions = new Vector2[vertexNumber];
        for (int j = 2; j < vertexNumber; j++)
        {
            for (int i = 2; i < vertexNumber - 1; i++)
            {
                if (positions[i + 1].x > positions[i].x)
                {
                    Vector2 aux = positions[i];
                    positions[i] = positions[i + 1];
                    positions[i + 1] = aux;
                }
            }
        }
        for (int i = 0; i < vertexNumber; i++)
        {
            orderedPositions[i] = positions[i];
        }

        return orderedPositions;
    }

    public static GameObject[] GetOrderedCars()
    {
        for (int i = 0; i < cars.Length; i++)
        {
            for (int j = 0; j < cars.Length - 1; j++)
            {
                if (cars[j + 1].GetComponent<Car>().GetScore() > cars[j].GetComponent<Car>().GetScore())
                {
                    GameObject aux = cars[j];
                    cars[j] = cars[j + 1];
                    cars[j + 1] = aux;
                }
            }
        }
        return cars;
    }

    public static int GetID()
    {
        return ++lastID;
    }
    
}
